var onMessageListener = function (message, sender, sendResponse) {
    switch (message.type) {
        case "bglog":
            chrome.tabs.executeScript(
                {
                    code: "document.getElementsByTagName('html')[0].innerHTML;"
                },
                function (ps1) {
                    var html = $(ps1[0]);
                    var enNumber = fixNumbers($(".d-inline-block.ml-2.cv-page-number b:last", html).text());
                    var pageSize = Math.floor(parseInt(enNumber) / 20) + 1;
                    chrome.tabs.query({ 'active': true, 'lastFocusedWindow': true }, function (tabs) {
                        var url = tabs[0].url;
                        if (url.indexOf("?") > 0) {
                            var url = url.substring(0, url.indexOf("?"));
                        }
                        callApi(pageSize, url);

                    });

                }
            );



            break;
    }
    return true;
}

var callApi = function (pageSize, url) {

    let promiseArr = [];
    let contenUrl = [];
    for (let i = 1; i < pageSize + 1; i++) {

        var promise = new Promise(function (resolve, reject) {
            (function (index) {
                $.ajax({
                    url: `${url}?page=${i}`,
                    type: 'GET',
                    dataType: 'html',
                    success: function (res) {
                        var html = $(res);
                        $("#parent table > tbody > tr", html).each(function (index, tr) {
                            var cUrl = $(tr).attr("data-candidate-box-url");
                            contenUrl.push(cUrl);
                        });
                        resolve();

                    },
                    error: function (err) {
                        console.log(err);
                        reject();
                    }
                });
            })(i)
        });

        promiseArr.push(promise);
    }

    Promise.all(promiseArr).then(function (values) {
        console.log(contenUrl);
    });

}

var fixNumbers = function (str) {
    var persianNumbers = [/۰/g, /۱/g, /۲/g, /۳/g, /۴/g, /۵/g, /۶/g, /۷/g, /۸/g, /۹/g];
    var arabicNumbers = [/٠/g, /١/g, /٢/g, /٣/g, /٤/g, /٥/g, /٦/g, /٧/g, /٨/g, /٩/g];
    if (typeof str === 'string') {
        for (var i = 0; i < 10; i++) {
            str = str.replace(persianNumbers[i], i).replace(arabicNumbers[i], i);
        }
    }
    return str;
};

chrome.runtime.onMessage.addListener(onMessageListener);
